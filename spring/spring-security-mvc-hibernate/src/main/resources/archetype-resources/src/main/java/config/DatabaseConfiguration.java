/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ${package}.config;

import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.IOException;

@Configuration
public class DatabaseConfiguration {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DatabaseConfiguration.class);

    @Bean
    @Primary
    public PlatformTransactionManager txManager() {
        final HibernateTransactionManager txManager = new HibernateTransactionManager(sessionFactory());
        txManager.setNestedTransactionAllowed(true);
        return txManager;
    }

    @Bean
    public SessionFactory sessionFactory() {
        final LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setConfigLocation(new ClassPathResource("hibernate.cfg.xml"));
        try {
            sessionFactoryBean.afterPropertiesSet();
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return sessionFactoryBean.getObject();
    }
}
