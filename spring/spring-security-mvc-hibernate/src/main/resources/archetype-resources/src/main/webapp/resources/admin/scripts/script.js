/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

$(document).ready(function () {
    reloadUsers();

    console.log("admin script executed!")
});


function reloadUsers() {
    var headers = {};
    headers[$('meta[name=_csrf_header]').attr("content")] = $('meta[name=_csrf]').attr("content");

    $.ajax({
        type: "GET",
        url: "api/admin/user/1",
        async: false,
        headers: headers,
        success: function (data) {

            var ptr = $("#divAdminUsers");

            ptr.empty();
            data.forEach(function (element) {
                ptr.append(`<span><span>${element.userId}</span>&nbsp;-&nbsp;<span>${element.username}</span>&nbsp;-&nbsp;<span>${element.role}</span></span><br>`)
            });
        }
    });
}

$("#buttonAdminAddUser").click(function () {
    var headers = {};
    headers[$('meta[name=_csrf_header]').attr("content")] = $('meta[name=_csrf]').attr("content");

    $.ajax({
        type: "POST",
        url: "api/admin/user/1",
        async: false,
        data: {
            username: $("#inputAdminNewUserUsr").val(),
            password: $("#inputAdminNewUserPwd").val(),
            role: $("input[name='role']:checked").val()
        },
        headers: headers,
        success: function (booleanResponse) {
            if (booleanResponse.value) {
                reloadUsers();
            }
        }
    });
});