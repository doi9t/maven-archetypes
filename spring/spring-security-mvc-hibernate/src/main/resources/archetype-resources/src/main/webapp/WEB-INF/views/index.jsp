<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%--
  ~    Copyright 2014 - 2018 Yannick Watier
  ~
  ~    Licensed under the Apache License, Version 2.0 (the "License");
  ~    you may not use this file except in compliance with the License.
  ~    You may obtain a copy of the License at
  ~
  ~        http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~    Unless required by applicable law or agreed to in writing, software
  ~    distributed under the License is distributed on an "AS IS" BASIS,
  ~    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~    See the License for the specific language governing permissions and
  ~    limitations under the License.
  --%>

<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="<c:url value="resources/css/style.css" />">
</head>
<body>
<h3>Login</h3>
<form action="${pageContext.request.contextPath}/login" method="post">
    <div class="container">

        <label><b>Username</b></label>
        <input type='text' placeholder="Enter Username" name='username' required><br>

        <label><b>Password</b></label>
        <input type='password' placeholder="Enter Password" name='password' required/><br>

        <button type="submit">Login</button>
    </div>

    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <script src="<c:url value="resources/scripts/jquery-3.2.1.min.js" />"></script>
    <script src="<c:url value="resources/scripts/script.js" />"></script>
    <script src="<c:url value="resources/admin/scripts/script.js" />"></script>
    <script src="<c:url value="resources/user/scripts/script.js" />"></script>
</form>
</body>
</html>
