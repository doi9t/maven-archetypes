/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ${package}.controller;

import ${package}.enums.Role;
import ${package}.pojo.NewUserPojo;
import ${package}.pojo.ViewableUserPojo;
import ${package}.rest.response.BooleanResponse;
import ${package}.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
public class AdminApiController {

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/user/1", method = RequestMethod.POST)
    public BooleanResponse createNewUser(@NotNull NewUserPojo newUserPojo, @NotNull Role role) {
        return BooleanResponse.getResponse(userService.createUserWithRole(newUserPojo, role));
    }

    @RequestMapping(path = "/user/1", method = RequestMethod.GET)
    public List<ViewableUserPojo> getUsers() {
        return userService.getAllUsers();
    }
}
