/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ${package}.service;

import ${package}.dao.UserDao;
import ${package}.entity.UserEntity;
import ${package}.enums.Role;
import ${package}.exception.UserException;
import ${package}.pojo.NewUserPojo;
import ${package}.pojo.ViewableUserPojo;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public boolean createNewUser(@NotNull NewUserPojo newUserPojo) {
        return createUserWithRole(newUserPojo, Role.ROLE_USER);
    }

    public boolean createUserWithRole(@NotNull NewUserPojo newUserPojo, @NotNull Role role) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(newUserPojo.getUsername());
        userEntity.setRole(role.name());
        userEntity.setUserHash(passwordEncoder.encode(newUserPojo.getPassword()));

        boolean isAddedCorrectly = false;

        try {
            userDao.addUser(userEntity);
            isAddedCorrectly = true;
        } catch (UserException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return isAddedCorrectly;
    }

    public boolean createNewAdmin(NewUserPojo newUserPojo) {
        return createUserWithRole(newUserPojo, Role.ROLE_ADMIN);
    }

    public List<ViewableUserPojo> getAllUsers() {
        List<ViewableUserPojo> values = new ArrayList<>();

        for (UserEntity userEntity : userDao.getAllUsers()) {
            values.add(new ViewableUserPojo(userEntity));
        }

        return values;
    }
}
