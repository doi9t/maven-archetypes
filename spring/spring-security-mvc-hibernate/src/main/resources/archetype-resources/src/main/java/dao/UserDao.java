/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ${package}.dao;

import ${package}.entity.UserEntity;
import ${package}.exception.UserAlreadyExistException;
import ${package}.exception.UserException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    public void addUser(@NotNull UserEntity user) throws UserException {

        String userName = user.getUserName();

        if (isUserExist(userName)) {
            throw new UserAlreadyExistException(userName);
        }

        this.sessionFactory.getCurrentSession().persist(user);
    }

    private boolean isUserExist(String username) {
        return getUserByUserName(username) != null;
    }

    /**
     * @param username - The username to be searched
     * @return
     */

    public UserEntity getUserByUserName(@NotNull String username) {

        Session currentSession = this.sessionFactory.getCurrentSession();

        Query query = currentSession.createQuery("FROM UserEntity where userName = :name");
        query.setParameter("name", username);

        return (UserEntity) query.uniqueResult();
    }


    public List<UserEntity> getAllUsers() {
        Session currentSession = this.sessionFactory.getCurrentSession();

        Query query = currentSession.createQuery("FROM UserEntity");

        return query.getResultList();
    }
}
